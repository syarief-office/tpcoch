/* Author:http://www.rainatspace.com

*/

function initializeScript(){
	
	//RESPONSIVE NAV
	jQuery("#menu").slicknav();
	
	//SLIDER
	jQuery('.bxslider').bxSlider({
		pager: false
	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
    //SLIDE MAP
    jQuery("#nav-map").click( function(){
		event.preventDefault();
		if (jQuery(this).hasClass("isDown") ) {
			jQuery("#footer-map").animate({bottom:'-250px'}, 200);			
			jQuery(this).removeClass("isDown");
		} else {
			jQuery("#footer-map").animate({bottom:'-480px'}, 200);	
			jQuery(this).addClass("isDown");
		}
		return false;
	});


	//EQUAL HEIGHT
	function equalHeight(group) {
		tallest = 0;
		jQuery(window).on("load resize", function(){
			if (jQuery(window).width() > 979  ) {
				group.each(function() {
					thisHeight = jQuery(this).height();
					if(thisHeight > tallest) {
						tallest = thisHeight;
					}
				});
				group.height(tallest);
			}
		});
	}
	equalHeight(jQuery(".eqHeight"));

	//jQuery(window).on("load resize", function(){
	//	if (jQuery(window).width() > 979  ) {
	//		hLeft = jQuery('heightLt');
	//		hRight = jQuery('heightRt');
	//
	//			if(hLeft > hRight){
	//
	//			}
	//		}
	//	});
});
/* END ------------------------------------------------------- */